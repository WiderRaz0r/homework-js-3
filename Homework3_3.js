alert(`Task 2: find all prime numbers in a given range`)
let userNum0 = +prompt('Enter lower number: ');
let userNum1 = +prompt('Enter higher number: ');
while (Number.isInteger(userNum0) == false  ||  userNum0 == undefined){
    alert(`Wrong number! Pls, enter an integer`)
    userNum0 = +prompt('Enter lower number, pls:');
}
while(Number.isInteger(userNum1) == false || userNum1 == undefined){
    alert(`Wrong number! Pls, enter an integer`)
    userNum1 = +prompt('Enter higher number, pls:');
}
console.log(`All prime numbers in a range between ${userNum0} and ${userNum1} are:`)
for (let i = userNum0; i <= userNum1; i++) {
    let primeDetector = 0;
    for (let j = 2; j < i; j++) {
        if (i % j == 0) {
            primeDetector = 1;
            break;
        }
    }
    if (i > 1 && primeDetector == 0) {
        console.log(i);
    }
}